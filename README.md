# X Validator Rule Editor

<!-- TOC -->
* [X Validator Rule Editor](#x-validator-rule-editor)
  * [Build and tests](#build-and-tests)
  * [Installation manual](#installation-manual)
    * [Configuration for sso-client-v7](#configuration-for-sso-client-v7)
  * [User manual](#user-manual)
  * [Release note](#release-note)
<!-- TOC -->

## Build and tests

The project must be build using JDK-8 and run on a JDK-7 environment.

```shell
mvn clean install
```

## Installation manual

The X Validator Rule Editor installation manual is available here:
https://gazelle.ihe.net/gazelle-documentation/Gazelle-X-Validator-Rule-Editor/installation.html

### Configuration for sso-client-v7

As X Validator Rule Editor depends on sso-client-v7, it needs to be configured properly to work. You can follow the README of sso-client-v7
available here: [sso-client-v7](https://gitlab.inria.fr/gazelle/library/sso-client-v7)

## User manual

The X Validator Rule Editor user manual is available here:
https://gazelle.ihe.net/gazelle-documentation/Gazelle-X-Validator-Rule-Editor/user.html

## Release note

The X Validator Rule Editor release note is available here:
https://gazelle.ihe.net/gazelle-documentation/Gazelle-X-Validator-Rule-Editor/release-note.html