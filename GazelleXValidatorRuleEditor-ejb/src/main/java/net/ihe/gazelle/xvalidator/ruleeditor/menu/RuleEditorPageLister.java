package net.ihe.gazelle.xvalidator.ruleeditor.menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.kohsuke.MetaInfServices;

import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.common.pages.PageLister;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;

@MetaInfServices(PageLister.class)
public class RuleEditorPageLister implements PageLister {

	@Override
	public Collection<Page> getPages() {
		Collection<Page> pages = new ArrayList<Page>();
		pages.addAll(Arrays.asList(XValidationPages.values()));
		pages.addAll(Arrays.asList(Pages.values()));
		return pages;
	}

}
