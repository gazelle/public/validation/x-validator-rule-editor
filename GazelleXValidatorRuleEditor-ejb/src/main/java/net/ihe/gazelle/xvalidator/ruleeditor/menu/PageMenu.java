package net.ihe.gazelle.xvalidator.ruleeditor.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public class PageMenu implements Page {

	private static int iMenu = 0;

	private String title;
	private String img;
	private String id;

	private Authorization[] authorizations;

	public PageMenu(String title, String img, Authorization... authorizations) {
		this(title, img);
		this.authorizations = authorizations;
	}

	public PageMenu(String title, String img) {
		super();
		this.title = title;
		this.img = img;
		this.id = "menu_" + iMenu;
		iMenu++;
		this.authorizations = new Authorization[] { Authorizations.ALL };
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getLabel() {
		return title;
	}

	@Override
	public String getLink() {
		return "";
	}

	@Override
	public String getIcon() {
		return img;
	}

	@Override
	public Authorization[] getAuthorizations() {
		return authorizations;
	}

	@Override
	public String getMenuLink() {
		return getLink();
	}

}
