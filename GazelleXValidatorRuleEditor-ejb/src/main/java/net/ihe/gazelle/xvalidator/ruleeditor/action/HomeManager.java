package net.ihe.gazelle.xvalidator.ruleeditor.action;

import java.io.Serializable;

import net.ihe.gazelle.xvalidator.ruleeditor.model.Home;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;


@Name("homeManagerBean")
@Scope(ScopeType.PAGE)
public class HomeManager implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1111092307583013570L;
	
	
	private Home selectedHome;
	private boolean mainContentEditMode;
	private boolean editTitle = false;

	public boolean isEditTitle() {
		return editTitle;
	}

	public void setEditTitle(boolean editTitle) {
		this.editTitle = editTitle;
	}
	
	public Home getSelectedHome() {
		return selectedHome;
	}

	public void setSelectedHome(Home selectedHome) {
		this.selectedHome = selectedHome;
	}

	public boolean isMainContentEditMode() {
		return mainContentEditMode;
	}

	public void setMainContentEditMode(boolean mainContentEditMode) {
		this.mainContentEditMode = mainContentEditMode;
	}

	@Create
	public void initializeHome()
	{
		selectedHome = Home.getHomeForSelectedLanguage();
	}
	
	
	public void editMainContent()
	{
		mainContentEditMode = true;
	}
	
	
	
	public void save()
	{
		selectedHome = selectedHome.save();
		mainContentEditMode = false;
		editTitle = false;
	}
	
	public void cancel()
	{
		mainContentEditMode = false;
		editTitle = false;
	}

}
