package net.ihe.gazelle.xvalidator.ruleeditor.menu;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.menu.Menu;
import net.ihe.gazelle.common.pages.menu.MenuEntry;
import net.ihe.gazelle.common.pages.menu.MenuGroup;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;

public class MainMenu {

	private static MenuGroup MENU = null;

	public static Menu getMenu() {
		if (MENU == null) {
			synchronized (MainMenu.class) {
				if (MENU == null) {
					MENU = computeMenu();
				}
			}
		}
		return MENU;
	}

	private static MenuGroup computeMenu() {
		// all elements
		ArrayList<Menu> children = new ArrayList<Menu>();

		// Home

		List<Menu> xvalidation = new ArrayList<Menu>();
		xvalidation.add(new MenuEntry(XValidationPages.VAL_VALIDATION));
		xvalidation.add(new MenuEntry(XValidationPages.VAL_LOGS));
		xvalidation.add(new MenuEntry(XValidationPages.DOC_HOME));
		addMenu(children, xvalidation, "Validation", "fa-play", Authorizations.EDITOR);
		
		List<Menu> creation = new ArrayList<Menu>();
		creation.add(new MenuEntry(XValidationPages.ADMIN_VALIDATORS));
		creation.add(new MenuEntry(XValidationPages.ADMIN_RULES));
		creation.add(new MenuEntry(XValidationPages.ADMIN_REFERENCES));
		creation.add(new MenuEntry(XValidationPages.ADMIN_IMPORT_VALIDATORS));
		creation.add(new MenuEntry(XValidationPages.ADMIN_ARCHIVES));
		List<Menu> testing = new ArrayList<Menu>();
		testing.add(new MenuEntry(XValidationPages.ADMIN_BROWSE_UT));
		testing.add(new MenuEntry(XValidationPages.ADMIN_RULE_UNIT_TEST_LOGS));
		addMenu(creation, testing, "Unit Test of rules", "fa-check-square-o", Authorizations.EDITOR);
		addMenu(children, creation, "Rule Editor", "fa-pencil", Authorizations.EDITOR);

		return new MenuGroup(null, children);
	}
	
	private static void addMenu(List<Menu> parentItems, List<Menu> items, String title, String img,
			Authorization... authorizations) {
		parentItems.add(new MenuGroup(new PageMenu(title, img, authorizations), items));
	}
}
