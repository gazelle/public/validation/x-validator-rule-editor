/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidator.ruleeditor.action;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.servletfilter.CSPHeaderFilter;
import net.ihe.gazelle.common.servletfilter.CSPPoliciesPreferences;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidator.ruleeditor.model.ApplicationConfiguration;
import net.ihe.gazelle.xvalidator.ruleeditor.model.ApplicationConfigurationQuery;
import net.ihe.gazelle.xvalidator.ruleeditor.providers.ApplicationPreferenceProvider;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Name("applicationManager")
@Scope(ScopeType.PAGE)
@MetaInfServices(CSPPoliciesPreferences.class)
public class ApplicationManager implements Serializable, CSPPoliciesPreferences {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private static Logger log = LoggerFactory.getLogger(ApplicationManager.class);
	
	private ApplicationConfiguration preference = null;

	public List<ApplicationConfiguration> getAllPreferences()
	{
		HQLQueryBuilder<ApplicationConfiguration> builder = 
			new HQLQueryBuilder<ApplicationConfiguration>((EntityManager)Component.getInstance("entityManager"),
															ApplicationConfiguration.class);
		builder.addOrder("variable", true);
		return builder.getList();
	}
	
	public void savePreference(ApplicationConfiguration inPreference)
	{
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		entityManager.merge(inPreference);
		entityManager.flush();
		preference = null;
		// reset preferences with scope application
		ApplicationPreferenceProvider.instance().init();
	}
	
	public void createNewPreference()
	{
		preference = new ApplicationConfiguration();
	}

	public void setPreference(ApplicationConfiguration preference) {
		this.preference = preference;
	}

	public ApplicationConfiguration getPreference() {
		return preference;
	}

	@Override
	public boolean isContentPolicyActivated() {
		return PreferenceService.getBoolean("security_policies");
	}

	@Override
	public Map<String, String> getHttpSecurityPolicies() {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(PreferencesKey.SECURITY_POLICIES.getFriendlyName(),
				String.valueOf(PreferenceService.getBoolean(PreferencesKey.SECURITY_POLICIES.getFriendlyName())));
		headers.put(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName(),
				PreferenceService.getString(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName()));
		headers.put(PreferencesKey.CACHE_CONTROL.getFriendlyName(),
				PreferenceService.getString(PreferencesKey.CACHE_CONTROL.getFriendlyName()));
		headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName(),
				PreferenceService.getString(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
		headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName(),
				PreferenceService.getString(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY
								.getFriendlyName()));
		headers.put(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName(),
				PreferenceService.getString(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName()));
		headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_CHROME.getFriendlyName(),
				PreferenceService.getString(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
		headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY_CHROME.getFriendlyName(),
				PreferenceService.getString(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY
								.getFriendlyName()));
		return headers;
	}

	@Override
	public boolean getSqlInjectionFilterSwitch() {
		return PreferenceService.getBoolean(PreferencesKey.SQL_INJECTION_FILTER_SWITCH.getFriendlyName());
	}
	
	public void resetHttpHeaders() {
		log.info("Reset http headers to default values");
		EntityManager entityManager = EntityManagerService.provideEntityManager();

		ApplicationConfiguration ap = null;
		for (PreferencesKey preference : PreferencesKey.values()) {
			ApplicationConfigurationQuery applicationConfigurationQuery = new ApplicationConfigurationQuery();
			String friendlyName = preference.getFriendlyName();
			applicationConfigurationQuery.variable().eq(friendlyName);
			ap = applicationConfigurationQuery.getUniqueResult();

			if (ap == null) {
				ap = new ApplicationConfiguration();
			}
			ap.setVariable(friendlyName);
			ap.setValue(preference.getDefaultValue());
			entityManager.merge(ap);
			entityManager.flush();
		}
		CSPHeaderFilter.clearCache();
	}
	
	public void updateHttpHeaders(){
		CSPHeaderFilter.clearCache();
	}
	
	public String getApplicationUrlBaseName(){
		return PreferenceService.getString("application_base_name");
	}
}
