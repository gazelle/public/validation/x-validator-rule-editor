package net.ihe.gazelle.xvalidator.ruleeditor.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.ihe.gazelle.hql.HQLQueryBuilder;

import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.web.Locale;

@Entity
@Name("home")
@Table(name="cmn_home", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="iso3_language"))
@SequenceGenerator(name="cmn_home_sequence", sequenceName="cmn_home_id_seq", allocationSize=1)
public class Home implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1218389994561421605L;

	@Id
	@GeneratedValue(generator="cmn_home_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	@Column(name="id", nullable=false, unique=true)
	private Integer id;
	
	@Column(name="iso3_language")
	private String iso3Language;
	
	@Column(name="main_content")
	@Lob
	@Type(type = "text")
	private String mainContent;
	
	@Column(name="home_title")
	private String homeTitle;
	
	public Home()
	{
		
	}
	
	public Home(String iso3Language)
	{
		this.iso3Language = iso3Language;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIso3Language() {
		return iso3Language;
	}

	public void setIso3Language(String iso3Language) {
		this.iso3Language = iso3Language;
	}

	public String getMainContent() {
		return mainContent;
	}

	public void setMainContent(String mainContent) {
		this.mainContent = mainContent;
	}

	public String getHomeTitle() {
		return homeTitle;
	}

	public void setHomeTitle(String homeTitle) {
		this.homeTitle = homeTitle;
	}

	/**
	 * 
	 * @return the saved home page
	 */
	public Home save()
	{
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		Home home = entityManager.merge(this);
		entityManager.flush();
		return home;
	}
	
	/**
	 * 
	 * @return the home page for the selected language
	 */
	public static Home getHomeForSelectedLanguage()
	{
		String language = Locale.instance().getISO3Language();
		if (language != null && !language.isEmpty())
		{
			EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
			HQLQueryBuilder<Home> queryBuilder = new HQLQueryBuilder<Home>(entityManager, Home.class);
			queryBuilder.addEq("iso3Language", language);
			List<Home> homes = queryBuilder.getList();
			if (homes != null && !homes.isEmpty())
			{
				return homes.get(0);
			}
			else
			{
				return new Home(language);
			}
		}
		else
		{
			return null;
		}
	}
}
