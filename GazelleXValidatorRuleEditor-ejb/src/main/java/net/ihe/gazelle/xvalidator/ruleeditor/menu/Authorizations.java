package net.ihe.gazelle.xvalidator.ruleeditor.menu;

import net.ihe.gazelle.common.pages.Authorization;

import org.jboss.seam.security.Identity;

public enum Authorizations implements Authorization {

	ALL,

	LOGGED,

	ADMIN,

	EDITOR;

	@Override
	public boolean isGranted(Object... context) {
		switch (this) {
		case ALL:
			return true;
		case LOGGED:
			return Identity.instance().isLoggedIn();
		case ADMIN:
			return Identity.instance().hasRole("admin_role");
		case EDITOR:
			return Identity.instance().hasRole("admin_role") || Identity.instance().hasRole("tests_editor_role");
		}
		return false;
	}

}
