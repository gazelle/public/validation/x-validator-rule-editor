package net.ihe.gazelle.xvalidator.ruleeditor.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public enum Pages implements
		Page {

	APPLICATION_CONF("/admin/configure.xhtml", "fa-wrenchf", "Application configuration", Authorizations.ADMIN),
	
	LOGIN_PAGE ("/cas/login.xhtml", "fa-sign-in", "", Authorizations.ALL),

	ERROR_PAGE ("/error.xhtml", "fa-exclamation-triangle", "", Authorizations.ALL),
	
	DEBUG_PAGE ("/debug.seam", "fa-exclamation-triangle", "", Authorizations.ALL),
	
	ERROR_EXPIRED_PAGE ("/errorExpired.xhtml", "fa-exclamation-triangle", "", Authorizations.ALL),

    HOME_PAGE ("/home.xhtml", "", "", Authorizations.ALL);

	
	private String link;

	private Authorization[] authorizations;

	private String label;

	private String icon;

	Pages(String link, String icon, String label, Authorization... authorizations) {
		this.link = link;
		this.authorizations = authorizations;
		this.label = label;
		this.icon = icon;
	}

	@Override
	public String getId() {
		return name();
	}

	@Override
	public String getMenuLink() {
		return link.replace(".xhtml",".seam");
	}

	@Override
	public String getLink() {
		return link;
	}

	@Override
	public String getIcon() {
		return icon;
	}

	@Override
	public Authorization[] getAuthorizations() {
		return authorizations;
	}

	@Override
	public String getLabel() {
		return label;
	}

}
