package net.ihe.gazelle.xvalidator.ruleeditor.providers;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.pages.menu.Menu;
import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.xvalidator.ruleeditor.menu.MainMenu;
import net.ihe.gazelle.xvalidator.ruleeditor.model.ApplicationConfiguration;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;
import org.kohsuke.MetaInfServices;

import javax.ejb.Remove;
import java.util.Date;

@MetaInfServices(PreferenceProvider.class)
@AutoCreate
@Name("applicationPreferenceProvider")
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationPreferenceProviderLocal")
public class ApplicationPreferenceProvider implements PreferenceProvider, ApplicationPreferenceProviderLocal {

	private Boolean applicationWorksWithoutCas;
	private Boolean ipLogin;
	private String ipLoginAdmin;
	private String applicationUrl;
	private String casUrl;

	@Create
	public void init() {
		applicationWorksWithoutCas = null;
		ipLogin = null;
		ipLoginAdmin = null;
		casUrl = null;
		applicationUrl = null;
	}

	@Destroy
	@Remove
	public void destroy() {
		init();
	}

	public static ApplicationPreferenceProviderLocal instance() {
		return (ApplicationPreferenceProviderLocal) Component.getInstance("applicationPreferenceProvider");
	}

	public Boolean getApplicationWorksWithoutCas() {
		if (applicationWorksWithoutCas == null) {
			applicationWorksWithoutCas  = getBoolean("application_works_without_cas");
		}
		return applicationWorksWithoutCas;
	}

	public String loginByIP() {
        Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
        if ("loggedIn".equals(identity.login())) {
            return "/home.xhtml";
        } else {
            return null;
        }
	}

	public Boolean getIpLogin() {
		if (ipLogin == null){
			ipLogin = getBoolean("ip_login");
		}
		return ipLogin;
	}

	public String getIpLoginAdmin() {
		if (ipLoginAdmin == null) {
			ipLoginAdmin = getString("ip_login_admin");
		}
		return ipLoginAdmin;
	}

	public String getApplicationUrl() {
		if (applicationUrl == null) {
			applicationUrl = getString("application_url");
		}
		return applicationUrl;
	}


	public boolean isUserAllowedAsAdmin() {
		return Identity.instance().isLoggedIn() && Identity.instance().hasRole("admin_role");
	}

	public Menu getMenu() {
		return MainMenu.getMenu();
	}
	
	@Override
	public int compareTo(PreferenceProvider o) {
		return getWeight().compareTo(o.getWeight());
	}

	@Override
	public Boolean getBoolean(String key) {
		String prefAsString = getString(key);
		if ((prefAsString != null) && !prefAsString.isEmpty()) {
			return Boolean.valueOf(prefAsString);
		} else {
			return false;
		}
	}

	@Override
	public Date getDate(String arg0) {
		return null;
	}

	@Override
	public Integer getInteger(String key) {
		String prefAsString = getString(key);
		if ((prefAsString != null) && !prefAsString.isEmpty()) {
			try {
				return Integer.decode(prefAsString);
			} catch (NumberFormatException e) {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public Object getObject(Object arg0) {
		return null;
	}

	@Override
	public String getString(String key) {
		return ApplicationConfiguration.getValueOfVariable(key);
	}

	@Override
	public Integer getWeight() {
		if (Contexts.isApplicationContextActive()) {
			return -100;
		} else {
			return 100;
		}
	}

	@Override
	public void setBoolean(String arg0, Boolean arg1) {

	}

	@Override
	public void setDate(String arg0, Date arg1) {

	}

	@Override
	public void setInteger(String arg0, Integer arg1) {

	}

	@Override
	public void setObject(Object arg0, Object arg1) {

	}

	@Override
	public void setString(String arg0, String arg1) {
	}

	public ApplicationPreferenceProvider() {
		super();
	}

}
