package net.ihe.gazelle.xvalidator.ruleeditor.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import net.ihe.gazelle.hql.HQLQueryBuilder;

import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

/**
 * @author Abderrazek Boufahja
 * 
 */
@Entity
@Name("applicationConfiguration")
@Table(name = "app_configuration", schema = "public")
@SequenceGenerator(name = "app_configuration_sequence", sequenceName = "app_configuration_id_seq", allocationSize = 1)
public class ApplicationConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5444985974251904654L;

	// attributes ////////////////////////////////////////////////////////////////////////////

	/** Id of this object */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_configuration_sequence")
	private Integer id;

	@Column(name = "variable")
	private String variable;

	@Column(name = "value")
	private String value;

	// constructors ////////////////////////////////////////////////////////////////////////////

	public ApplicationConfiguration() {
	}

	public ApplicationConfiguration(String variable, String value) {
		super();
		this.variable = variable;
		this.value = value;
	}

	// getters and setters //////////////////////////////////////////////////////////////////////

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	// hashcode and equals //////////////////////////////////////////////////////////////////////

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((value == null) ? 0 : value.hashCode());
		result = (prime * result) + ((variable == null) ? 0 : variable.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ApplicationConfiguration other = (ApplicationConfiguration) obj;
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		if (variable == null) {
			if (other.variable != null) {
				return false;
			}
		} else if (!variable.equals(other.variable)) {
			return false;
		}
		return true;
	}

	// methods ///////////////////////////////////////////////////////////////////////////////////

	@SuppressWarnings("unchecked")
	public static String getValueOfVariable(String variable) {
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		return getValueOfVariable(variable, entityManager);
	}

	public static String getValueOfVariable(String variable, EntityManager entityManager) {
		HQLQueryBuilder<ApplicationConfiguration> builder = new HQLQueryBuilder<ApplicationConfiguration>(
				entityManager, ApplicationConfiguration.class);
		builder.addEq("variable", variable);
		ApplicationConfiguration app = builder.getUniqueResult();
		if (app != null) {
			return app.getValue();
		} else {
			return null;
		}
	}

	public static ApplicationConfiguration getApplicationConfigurationByVariable(String variable) {
		HQLQueryBuilder<ApplicationConfiguration> builder = new HQLQueryBuilder<ApplicationConfiguration>(
				ApplicationConfiguration.class);
		builder.addEq("variable", variable);
		return builder.getUniqueResult();
	}

	public void save() {
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		entityManager.merge(this);
		entityManager.flush();
	}

	public String getApplicationUrl() {
		return getValueOfVariable("application_url");
	}

}
