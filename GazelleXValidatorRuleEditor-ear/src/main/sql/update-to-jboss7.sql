INSERT INTO app_configuration (id, variable, "value") values (nextval('app_configuration_id_seq'), 'application_base_name', 'GazelleXValidatorRuleEditor');
INSERT INTO app_configuration (id, variable, "value") values (nextval('app_configuration_id_seq'), 'NUMBER_OF_ITEMS_PER_PAGE', '20');
ALTER TABLE xval_uploaded_file_files DROP COLUMN uploaded_file_path;
UPDATE xval_validator_configuration SET assertion_manager_url = 'https://gazelle.ihe.net/AssertionManagerGui';