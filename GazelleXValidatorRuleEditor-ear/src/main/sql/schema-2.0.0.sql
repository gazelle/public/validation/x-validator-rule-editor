--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE public.app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: inputs_with_file_keywords_ut; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.inputs_with_file_keywords_ut (
    tested_rule_id integer NOT NULL,
    inputs_with_file_keywords character varying(255)
);


ALTER TABLE public.inputs_with_file_keywords_ut OWNER TO gazelle;

--
-- Name: ut_test_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.ut_test_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ut_test_file_id_seq OWNER TO gazelle;

--
-- Name: ut_unit_test; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.ut_unit_test (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    active boolean NOT NULL,
    comment text,
    keyword character varying(255) NOT NULL,
    last_modifier character varying(255),
    last_result boolean,
    last_run_date timestamp without time zone,
    last_success_date timestamp without time zone,
    last_tested_version character varying(255),
    reason_for_failure text,
    tested_rule_id integer,
    expected_result character varying(255)
);


ALTER TABLE public.ut_unit_test OWNER TO gazelle;

--
-- Name: ut_unit_test_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.ut_unit_test_file (
    id integer NOT NULL,
    comment text,
    filepath character varying(255),
    input_keyword character varying(255),
    input_type integer,
    unit_test_id integer
);


ALTER TABLE public.ut_unit_test_file OWNER TO gazelle;

--
-- Name: ut_unit_test_file_log; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.ut_unit_test_file_log (
    log_id integer NOT NULL,
    file_id integer NOT NULL
);


ALTER TABLE public.ut_unit_test_file_log OWNER TO gazelle;

--
-- Name: ut_unit_test_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.ut_unit_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ut_unit_test_id_seq OWNER TO gazelle;

--
-- Name: ut_unit_test_log; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.ut_unit_test_log (
    id integer NOT NULL,
    additional_info text,
    effective_result integer,
    reason_for_failure text,
    run_date timestamp without time zone,
    success boolean NOT NULL,
    tested_version character varying(255),
    unit_test_id integer
);


ALTER TABLE public.ut_unit_test_log OWNER TO gazelle;

--
-- Name: ut_unit_test_log_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.ut_unit_test_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ut_unit_test_log_id_seq OWNER TO gazelle;

--
-- Name: xval_assertion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_assertion (
    id integer NOT NULL,
    assertion_id character varying(255) NOT NULL,
    id_scheme character varying(255) NOT NULL
);


ALTER TABLE public.xval_assertion OWNER TO gazelle;

--
-- Name: xval_assertion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_assertion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_assertion_id_seq OWNER TO gazelle;

--
-- Name: xval_assertion_rule; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_assertion_rule (
    assertion_id integer NOT NULL,
    rule_id integer NOT NULL
);


ALTER TABLE public.xval_assertion_rule OWNER TO gazelle;

--
-- Name: xval_cross_validation_log; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_cross_validation_log (
    id integer NOT NULL,
    affinity_domain character varying(255),
    detailed_result text,
    gazelle_cross_validator character varying(255),
    is_public boolean,
    oid character varying(255),
    privacy_key character varying(255),
    result integer,
    "timestamp" timestamp without time zone,
    username character varying(255),
    calling_tool_oid character varying(255),
    external_id character varying(255),
    message_type_proxy character varying(255)
);


ALTER TABLE public.xval_cross_validation_log OWNER TO gazelle;

--
-- Name: xval_cross_validation_log_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_cross_validation_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_cross_validation_log_id_seq OWNER TO gazelle;

--
-- Name: xval_cross_validator; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_cross_validator (
    id integer NOT NULL,
    affinitydomain character varying(255),
    description text,
    last_modified_date timestamp without time zone,
    last_modifier character varying(255),
    name character varying(255),
    status integer,
    version character varying(255),
    oid character varying(255)
);


ALTER TABLE public.xval_cross_validator OWNER TO gazelle;

--
-- Name: xval_cross_validator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_cross_validator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_cross_validator_id_seq OWNER TO gazelle;

--
-- Name: xval_cross_validator_parent; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_cross_validator_parent (
    validator_id integer NOT NULL,
    validator_parent_id integer NOT NULL
);


ALTER TABLE public.xval_cross_validator_parent OWNER TO gazelle;

--
-- Name: xval_cross_validator_reference; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_cross_validator_reference (
    id integer NOT NULL,
    affinity_domain character varying(255),
    name character varying(255),
    version character varying(255)
);


ALTER TABLE public.xval_cross_validator_reference OWNER TO gazelle;

--
-- Name: xval_cross_validator_reference_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_cross_validator_reference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_cross_validator_reference_id_seq OWNER TO gazelle;

--
-- Name: xval_deprecated_validator_version; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_deprecated_validator_version (
    id integer NOT NULL,
    affinity_domain character varying(255),
    comment text,
    file_path character varying(255),
    last_modified timestamp without time zone,
    last_modifier character varying(255),
    name character varying(255),
    version character varying(255)
);


ALTER TABLE public.xval_deprecated_validator_version OWNER TO gazelle;

--
-- Name: xval_deprecated_validator_version_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_deprecated_validator_version_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_deprecated_validator_version_id_seq OWNER TO gazelle;

--
-- Name: xval_expression; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_expression (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    math_comparator integer,
    delta double precision,
    basic_operator integer,
    logical_operator integer,
    lang character varying(255),
    value_set_id character varying(255),
    var character varying(255),
    left_member_id integer,
    right_member_id integer,
    xpath_id integer,
    expression_id integer,
    locator_1_id integer,
    locator_2_id integer,
    locator_id integer,
    expression_set_id integer
);


ALTER TABLE public.xval_expression OWNER TO gazelle;

--
-- Name: xval_expression_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_expression_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_expression_id_seq OWNER TO gazelle;

--
-- Name: xval_locator; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_locator (
    id integer NOT NULL,
    applies_on character varying(255) NOT NULL,
    path text NOT NULL
);


ALTER TABLE public.xval_locator OWNER TO gazelle;

--
-- Name: xval_locator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_locator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_locator_id_seq OWNER TO gazelle;

--
-- Name: xval_log_id; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_log_id (
    log_id integer,
    id integer NOT NULL
);


ALTER TABLE public.xval_log_id OWNER TO gazelle;

--
-- Name: xval_logical_operation_expression_set; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_logical_operation_expression_set (
    xval_expression_id integer NOT NULL,
    expressionset_id integer NOT NULL,
    xval_expression_set_id integer NOT NULL
);


ALTER TABLE public.xval_logical_operation_expression_set OWNER TO gazelle;

--
-- Name: xval_math_member; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_math_member (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    math_operator integer,
    value double precision,
    member1_id integer,
    member2_id integer,
    locator_id integer
);


ALTER TABLE public.xval_math_member OWNER TO gazelle;

--
-- Name: xval_math_member_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_math_member_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_math_member_id_seq OWNER TO gazelle;

--
-- Name: xval_namespace; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_namespace (
    id integer NOT NULL,
    prefix character varying(255),
    uri character varying(255)
);


ALTER TABLE public.xval_namespace OWNER TO gazelle;

--
-- Name: xval_namespace_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_namespace_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_namespace_id_seq OWNER TO gazelle;

--
-- Name: xval_object_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_object_type (
    id integer NOT NULL,
    extension character varying(255),
    kind integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.xval_object_type OWNER TO gazelle;

--
-- Name: xval_object_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_object_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_object_type_id_seq OWNER TO gazelle;

--
-- Name: xval_referenced_object; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_referenced_object (
    id integer NOT NULL,
    description text,
    keyword character varying(255),
    object_type integer,
    xsdlocation character varying(255)
);


ALTER TABLE public.xval_referenced_object OWNER TO gazelle;

--
-- Name: xval_referenced_object_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_referenced_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_referenced_object_id_seq OWNER TO gazelle;

--
-- Name: xval_rule; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_rule (
    id integer NOT NULL,
    applies_to bytea,
    description text NOT NULL,
    keyword character varying(255) NOT NULL,
    last_modified_date timestamp without time zone,
    last_modifier character varying(255) NOT NULL,
    level integer NOT NULL,
    status integer NOT NULL,
    textual_expression text,
    version character varying(255) NOT NULL,
    expression_id integer NOT NULL,
    gazelle_x_validator_id integer
);


ALTER TABLE public.xval_rule OWNER TO gazelle;

--
-- Name: xval_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_rule_id_seq OWNER TO gazelle;

--
-- Name: xval_uploaded_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_uploaded_file (
    id integer NOT NULL,
    directory character varying(255),
    input_description character varying(255),
    keyword character varying(255),
    input_max integer,
    input_min integer,
    input_object_type integer
);


ALTER TABLE public.xval_uploaded_file OWNER TO gazelle;

--
-- Name: xval_uploaded_file_files; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_uploaded_file_files (
    xval_uploaded_file_id integer NOT NULL,
    element character varying(255)
);


ALTER TABLE public.xval_uploaded_file_files OWNER TO gazelle;

--
-- Name: xval_uploaded_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_uploaded_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_uploaded_file_id_seq OWNER TO gazelle;

--
-- Name: xval_validator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_validator_configuration (
    id integer NOT NULL,
    assertion_manager_url character varying(255),
    dicom_library integer,
    value_set_provider_url character varying(255),
    validator_id integer
);


ALTER TABLE public.xval_validator_configuration OWNER TO gazelle;

--
-- Name: xval_validator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_validator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_validator_configuration_id_seq OWNER TO gazelle;

--
-- Name: xval_validator_configuration_namespace; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_validator_configuration_namespace (
    configuration_id integer NOT NULL,
    namespace_id integer NOT NULL
);


ALTER TABLE public.xval_validator_configuration_namespace OWNER TO gazelle;

--
-- Name: xval_validator_input; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_validator_input (
    id integer NOT NULL,
    max_quantity integer,
    min_quantity integer,
    gazelle_cross_validator_id integer,
    referenced_object_id integer,
    message_type character varying(255)
);


ALTER TABLE public.xval_validator_input OWNER TO gazelle;

--
-- Name: xval_validator_input_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_validator_input_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_validator_input_id_seq OWNER TO gazelle;

--
-- Name: xval_validator_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_validator_usage (
    id integer NOT NULL,
    caller character varying(255),
    date date,
    status character varying(255),
    type character varying(255)
);


ALTER TABLE public.xval_validator_usage OWNER TO gazelle;

--
-- Name: xval_validator_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_validator_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_validator_usage_id_seq OWNER TO gazelle;

--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: xval_rule uk_1cf5qn39ldgfcb6t2orjobc63; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_rule
    ADD CONSTRAINT uk_1cf5qn39ldgfcb6t2orjobc63 UNIQUE (version, keyword);


--
-- Name: xval_assertion_rule uk_6vqu0bbk75hhc9045s2los5ef; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_assertion_rule
    ADD CONSTRAINT uk_6vqu0bbk75hhc9045s2los5ef UNIQUE (assertion_id, rule_id);


--
-- Name: ut_unit_test uk_89jnp3ftpf9x1fabhp0ld3g6y; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test
    ADD CONSTRAINT uk_89jnp3ftpf9x1fabhp0ld3g6y UNIQUE (keyword);


--
-- Name: xval_logical_operation_expression_set uk_ax6ugs1gj1apnnfv5fwny0p3r; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_logical_operation_expression_set
    ADD CONSTRAINT uk_ax6ugs1gj1apnnfv5fwny0p3r UNIQUE (expressionset_id);


--
-- Name: xval_cross_validator_parent uk_b48phy6ijhrg1lbypmrv7jxqv; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validator_parent
    ADD CONSTRAINT uk_b48phy6ijhrg1lbypmrv7jxqv UNIQUE (validator_parent_id, validator_id);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: xval_referenced_object uk_ovjkbvb6m0bhbkhqbibe6cipg; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_referenced_object
    ADD CONSTRAINT uk_ovjkbvb6m0bhbkhqbibe6cipg UNIQUE (keyword);


--
-- Name: xval_validator_configuration_namespace uk_p261236c5epprob8ilej79yy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_configuration_namespace
    ADD CONSTRAINT uk_p261236c5epprob8ilej79yy1 UNIQUE (configuration_id, namespace_id);


--
-- Name: ut_unit_test_file_log uk_p5f447j78nqr5q5njfp5kri80; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_file_log
    ADD CONSTRAINT uk_p5f447j78nqr5q5njfp5kri80 UNIQUE (file_id, log_id);


--
-- Name: xval_object_type uk_tkobc8f91qnv6pqxhsugdqlf1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_object_type
    ADD CONSTRAINT uk_tkobc8f91qnv6pqxhsugdqlf1 UNIQUE (name);


--
-- Name: ut_unit_test_file ut_unit_test_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_file
    ADD CONSTRAINT ut_unit_test_file_pkey PRIMARY KEY (id);


--
-- Name: ut_unit_test_log ut_unit_test_log_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_log
    ADD CONSTRAINT ut_unit_test_log_pkey PRIMARY KEY (id);


--
-- Name: ut_unit_test ut_unit_test_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test
    ADD CONSTRAINT ut_unit_test_pkey PRIMARY KEY (id);


--
-- Name: xval_assertion xval_assertion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_assertion
    ADD CONSTRAINT xval_assertion_pkey PRIMARY KEY (id);


--
-- Name: xval_cross_validation_log xval_cross_validation_log_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validation_log
    ADD CONSTRAINT xval_cross_validation_log_pkey PRIMARY KEY (id);


--
-- Name: xval_cross_validator xval_cross_validator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validator
    ADD CONSTRAINT xval_cross_validator_pkey PRIMARY KEY (id);


--
-- Name: xval_cross_validator_reference xval_cross_validator_reference_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validator_reference
    ADD CONSTRAINT xval_cross_validator_reference_pkey PRIMARY KEY (id);


--
-- Name: xval_deprecated_validator_version xval_deprecated_validator_version_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_deprecated_validator_version
    ADD CONSTRAINT xval_deprecated_validator_version_pkey PRIMARY KEY (id);


--
-- Name: xval_expression xval_expression_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT xval_expression_pkey PRIMARY KEY (id);


--
-- Name: xval_locator xval_locator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_locator
    ADD CONSTRAINT xval_locator_pkey PRIMARY KEY (id);


--
-- Name: xval_log_id xval_log_id_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_log_id
    ADD CONSTRAINT xval_log_id_pkey PRIMARY KEY (id);


--
-- Name: xval_logical_operation_expression_set xval_logical_operation_expression_set_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_logical_operation_expression_set
    ADD CONSTRAINT xval_logical_operation_expression_set_pkey PRIMARY KEY (xval_expression_set_id);


--
-- Name: xval_math_member xval_math_member_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_math_member
    ADD CONSTRAINT xval_math_member_pkey PRIMARY KEY (id);


--
-- Name: xval_namespace xval_namespace_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_namespace
    ADD CONSTRAINT xval_namespace_pkey PRIMARY KEY (id);


--
-- Name: xval_object_type xval_object_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_object_type
    ADD CONSTRAINT xval_object_type_pkey PRIMARY KEY (id);


--
-- Name: xval_referenced_object xval_referenced_object_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_referenced_object
    ADD CONSTRAINT xval_referenced_object_pkey PRIMARY KEY (id);


--
-- Name: xval_rule xval_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_rule
    ADD CONSTRAINT xval_rule_pkey PRIMARY KEY (id);


--
-- Name: xval_uploaded_file xval_uploaded_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_uploaded_file
    ADD CONSTRAINT xval_uploaded_file_pkey PRIMARY KEY (id);


--
-- Name: xval_validator_configuration xval_validator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_configuration
    ADD CONSTRAINT xval_validator_configuration_pkey PRIMARY KEY (id);


--
-- Name: xval_validator_input xval_validator_input_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_input
    ADD CONSTRAINT xval_validator_input_pkey PRIMARY KEY (id);


--
-- Name: xval_validator_usage xval_validator_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_usage
    ADD CONSTRAINT xval_validator_usage_pkey PRIMARY KEY (id);


--
-- Name: xval_validator_configuration_namespace fk_19x0hbg6r4ay0ubeh3bwlsp3u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_configuration_namespace
    ADD CONSTRAINT fk_19x0hbg6r4ay0ubeh3bwlsp3u FOREIGN KEY (namespace_id) REFERENCES public.xval_namespace(id);


--
-- Name: ut_unit_test fk_1hjjeam5i6l71jbftje43x6yn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test
    ADD CONSTRAINT fk_1hjjeam5i6l71jbftje43x6yn FOREIGN KEY (tested_rule_id) REFERENCES public.xval_rule(id);


--
-- Name: xval_expression fk_22ie064gm9erjye7x0j5658ao; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_22ie064gm9erjye7x0j5658ao FOREIGN KEY (right_member_id) REFERENCES public.xval_math_member(id);


--
-- Name: xval_log_id fk_2rahgovgpmx0utgokrjhmpe4t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_log_id
    ADD CONSTRAINT fk_2rahgovgpmx0utgokrjhmpe4t FOREIGN KEY (id) REFERENCES public.xval_uploaded_file(id);


--
-- Name: ut_unit_test_log fk_43gcwunrbx9ita0uugls13tx8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_log
    ADD CONSTRAINT fk_43gcwunrbx9ita0uugls13tx8 FOREIGN KEY (unit_test_id) REFERENCES public.ut_unit_test(id);


--
-- Name: xval_expression fk_4ndhx9ggnxgm63ye062iur8eb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_4ndhx9ggnxgm63ye062iur8eb FOREIGN KEY (locator_id) REFERENCES public.xval_locator(id);


--
-- Name: xval_math_member fk_4xbk50fvrv7g64nnefsyepc7w; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_math_member
    ADD CONSTRAINT fk_4xbk50fvrv7g64nnefsyepc7w FOREIGN KEY (member1_id) REFERENCES public.xval_math_member(id);


--
-- Name: xval_expression fk_6n0x2rg3maqbyjgsdqg3bshc2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_6n0x2rg3maqbyjgsdqg3bshc2 FOREIGN KEY (expression_id) REFERENCES public.xval_expression(id);


--
-- Name: xval_cross_validator_parent fk_6ugk5r645ycax1vlaxck13cvi; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validator_parent
    ADD CONSTRAINT fk_6ugk5r645ycax1vlaxck13cvi FOREIGN KEY (validator_parent_id) REFERENCES public.xval_cross_validator_reference(id);


--
-- Name: xval_logical_operation_expression_set fk_8u31doqfmu66majudya14xlsf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_logical_operation_expression_set
    ADD CONSTRAINT fk_8u31doqfmu66majudya14xlsf FOREIGN KEY (xval_expression_id) REFERENCES public.xval_expression(id);


--
-- Name: xval_log_id fk_9f26kefa9c0svy5i1sdixnrsg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_log_id
    ADD CONSTRAINT fk_9f26kefa9c0svy5i1sdixnrsg FOREIGN KEY (log_id) REFERENCES public.xval_cross_validation_log(id);


--
-- Name: xval_logical_operation_expression_set fk_ax6ugs1gj1apnnfv5fwny0p3r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_logical_operation_expression_set
    ADD CONSTRAINT fk_ax6ugs1gj1apnnfv5fwny0p3r FOREIGN KEY (expressionset_id) REFERENCES public.xval_expression(id);


--
-- Name: xval_rule fk_ba4yvkhb4yutvi5wvkgq6jd8o; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_rule
    ADD CONSTRAINT fk_ba4yvkhb4yutvi5wvkgq6jd8o FOREIGN KEY (expression_id) REFERENCES public.xval_expression(id);


--
-- Name: xval_expression fk_co24xyo1qpv11tqen99vfjglx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_co24xyo1qpv11tqen99vfjglx FOREIGN KEY (left_member_id) REFERENCES public.xval_math_member(id);


--
-- Name: xval_expression fk_dbdek6sy168h9xucgdwmuphnp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_dbdek6sy168h9xucgdwmuphnp FOREIGN KEY (locator_2_id) REFERENCES public.xval_locator(id);


--
-- Name: xval_validator_configuration fk_e11p1i3ndovl9eknuxh9vwj2h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_configuration
    ADD CONSTRAINT fk_e11p1i3ndovl9eknuxh9vwj2h FOREIGN KEY (validator_id) REFERENCES public.xval_cross_validator(id);


--
-- Name: xval_math_member fk_epaka7b4vh8da52bquik69x45; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_math_member
    ADD CONSTRAINT fk_epaka7b4vh8da52bquik69x45 FOREIGN KEY (member2_id) REFERENCES public.xval_math_member(id);


--
-- Name: xval_expression fk_evtx49btrdn5cl3kixbhu8uc4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_evtx49btrdn5cl3kixbhu8uc4 FOREIGN KEY (locator_1_id) REFERENCES public.xval_locator(id);


--
-- Name: ut_unit_test_file_log fk_h9i6r1hbkmv4f854l0kc0suya; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_file_log
    ADD CONSTRAINT fk_h9i6r1hbkmv4f854l0kc0suya FOREIGN KEY (log_id) REFERENCES public.ut_unit_test_log(id);


--
-- Name: xval_expression fk_hqbt43wnfcr9rqd8eglo4b3lk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_hqbt43wnfcr9rqd8eglo4b3lk FOREIGN KEY (xpath_id) REFERENCES public.xval_locator(id);


--
-- Name: xval_expression fk_me8smxc0qwetrec75evhd3ols; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_me8smxc0qwetrec75evhd3ols FOREIGN KEY (expression_set_id) REFERENCES public.xval_expression(id);


--
-- Name: ut_unit_test_file fk_mfwl6smcrvm48qietgxg2dpjf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_file
    ADD CONSTRAINT fk_mfwl6smcrvm48qietgxg2dpjf FOREIGN KEY (unit_test_id) REFERENCES public.ut_unit_test(id);


--
-- Name: ut_unit_test_file_log fk_n0piwt1fuuhu3fublnfe7s5r8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_file_log
    ADD CONSTRAINT fk_n0piwt1fuuhu3fublnfe7s5r8 FOREIGN KEY (file_id) REFERENCES public.ut_unit_test_file(id);


--
-- Name: inputs_with_file_keywords_ut fk_opsx8cuptuagh2hvbfagq3yla; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.inputs_with_file_keywords_ut
    ADD CONSTRAINT fk_opsx8cuptuagh2hvbfagq3yla FOREIGN KEY (tested_rule_id) REFERENCES public.ut_unit_test(id);


--
-- Name: xval_math_member fk_otlt6gkwq94wrf8lgvwwutvrp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_math_member
    ADD CONSTRAINT fk_otlt6gkwq94wrf8lgvwwutvrp FOREIGN KEY (locator_id) REFERENCES public.xval_locator(id);


--
-- Name: xval_assertion_rule fk_pc9wo48sbq9iadwfwqv42e402; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_assertion_rule
    ADD CONSTRAINT fk_pc9wo48sbq9iadwfwqv42e402 FOREIGN KEY (rule_id) REFERENCES public.xval_rule(id);


--
-- Name: xval_validator_input fk_pcryas065fb7ckp4ptaxgy62h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_input
    ADD CONSTRAINT fk_pcryas065fb7ckp4ptaxgy62h FOREIGN KEY (referenced_object_id) REFERENCES public.xval_referenced_object(id);


--
-- Name: xval_uploaded_file_files fk_pk86umabhm525rag7057alkqm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_uploaded_file_files
    ADD CONSTRAINT fk_pk86umabhm525rag7057alkqm FOREIGN KEY (xval_uploaded_file_id) REFERENCES public.xval_uploaded_file(id);


--
-- Name: xval_validator_configuration_namespace fk_pmuaetod2utrbeaujd2qi3de3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_configuration_namespace
    ADD CONSTRAINT fk_pmuaetod2utrbeaujd2qi3de3 FOREIGN KEY (configuration_id) REFERENCES public.xval_validator_configuration(id);


--
-- Name: xval_rule fk_rl2kqdy60m3vqrvg3xk6pxe4r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_rule
    ADD CONSTRAINT fk_rl2kqdy60m3vqrvg3xk6pxe4r FOREIGN KEY (gazelle_x_validator_id) REFERENCES public.xval_cross_validator(id);


--
-- Name: xval_cross_validator_parent fk_s25p6c28veqf8ht3x0tjjnl7t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validator_parent
    ADD CONSTRAINT fk_s25p6c28veqf8ht3x0tjjnl7t FOREIGN KEY (validator_id) REFERENCES public.xval_cross_validator(id);


--
-- Name: xval_assertion_rule fk_t0pmq9dirdqx12fs3ywkgvxel; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_assertion_rule
    ADD CONSTRAINT fk_t0pmq9dirdqx12fs3ywkgvxel FOREIGN KEY (assertion_id) REFERENCES public.xval_assertion(id);


--
-- Name: xval_validator_input fk_ta15jmuhsritupgp2u7xfwvl9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_input
    ADD CONSTRAINT fk_ta15jmuhsritupgp2u7xfwvl9 FOREIGN KEY (gazelle_cross_validator_id) REFERENCES public.xval_cross_validator(id);


--
-- PostgreSQL database dump complete
--

