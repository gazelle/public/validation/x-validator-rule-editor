UPDATE app_configuration SET value='1.2.3.4.5.6.6.6.2.' WHERE variable='xvalidation_root_oid';
UPDATE app_configuration SET variable='x_validation_log_root_oid' WHERE value='1.2.3.4.5.6.6.6.2.';

INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'x_validator_root_oid', '1.2.3.4.5.6.6.6.1.');

UPDATE xval_cross_validator SET oid=(SELECT value from app_configuration where variable LIKE 'x_validator_root_oid')|| id WHERE oid IS NULL;

UPDATE xval_validator_input SET message_type = 'REQUEST' WHERE referenced_object_id IN (SELECT id FROM xval_referenced_object WHERE keyword ilike '%request%');
UPDATE xval_validator_input SET message_type = 'RESPONSE' WHERE referenced_object_id IN (SELECT id FROM xval_referenced_object WHERE keyword ilike '%response%');
UPDATE xval_validator_input SET message_type = 'ATTACHMENT' WHERE referenced_object_id IN (SELECT id FROM xval_referenced_object WHERE keyword ilike '%attachment%');

INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'x_validation_report_xsl_url', 'https://gazelle.ihe.net/GazelleXValidatorRuleEditor/resources/stylesheet/report-mini.xsl');

ALTER TABLE ut_unit_test ADD COLUMN expected_result_string varchar(255);
UPDATE ut_unit_test SET expected_result_string = 'PASSED' where expected_result = 0;
UPDATE ut_unit_test SET expected_result_string = 'FAILED' where expected_result = 1;
UPDATE ut_unit_test SET expected_result_string = 'ABORTED' where expected_result = 2;
ALTER TABLE ut_unit_test DROP COLUMN expected_result;
ALTER TABLE ut_unit_test RENAME COLUMN expected_result_string TO expected_result;